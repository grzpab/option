import 'mocha';
import { expect } from 'chai';
import { TryFactory } from '../src';

describe('/Try.ts', () => {
    type A = {
        b: boolean;
    };

    it('returns the value', () => {
        const supplier = () => ({ b: true });
        const t = TryFactory.of<A>(supplier);

        const result = t.getOrElse({ b: false });

        expect(result.b).to.be.true;
    });

    it('returns the alternate value', () => {
        const supplier = () => {
            throw new Error('test error');
        };
        const t = TryFactory.of<A>(supplier);

        const result = t.getOrElse({ b: false });

        expect(result.b).to.be.false;
    });

    it('returns the value when using get()', () => {
        const supplier = () => ({ b: true });
        const t = TryFactory.of<A>(supplier);

        const result = t.get();

        expect(result.b).to.be.true;
    });

    it('throws an exception when using get()', () => {
        const supplier = () => {
            throw new Error('test error');
        };

        const t = TryFactory.of<A>(supplier);

        const callback = () => t.get();

        expect(callback).to.throw('test error');
    });

    it('reduces Try<Try<T>> into Try<T>', () => {
        const basicSupplier = () => ({ b: true });

        const supplier = () => {
            return TryFactory.of<A>(basicSupplier);
        };

        const firstT = TryFactory.of(supplier);
        const t = TryFactory.reduce<A>(firstT);

        expect(t.get()).to.deep.equal({b: true});
    });
});