import 'mocha';
import { expect } from 'chai';
import { OptionFactory } from '../src';

describe('/Option.ts', () => {
    type C = {
        d?: boolean;
    };
    
    type B = {
        c?: C; 
    };
    
    type A = {
        b?: B;
    };

    it('returns true', () => {
        const obj: A = {
            b: {
                c: {
                    d: true,
                }
            }
        };

        const result = OptionFactory.of(obj)
            .collect(a => a.b)
            .collect(b => b.c)
            .collect(c => c.d)
            .getOrElse(false);

        expect(result).to.be.true;
    });

    it('returns false', () => {
        const obj: A = {};

        const result = OptionFactory.of(obj)
            .collect(a => a.b)
            .collect(b => b.c)
            .collect(c => c.d)
            .getOrElse(false);

        expect(result).to.be.false;
    });
});