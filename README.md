# Option

A typesafe implementation of the Option(al) and other monads (not full yet).

Inspired by the VAVR library: http://www.vavr.io

## How to use
In order to extract nested values in objects, we can use non-type safe libraries like LowDash or use this subtle trick:
```
type A = {
    b?: {
        c?: {
            d?: boolean
        }
    }
};

const obj: A = {};

// const result = _.get(a, 'b.c.d', false)
// above works but not typesafe

const result: boolean = Option.of(obj)
    .collect(a => a.b)
    .collect(b => b.c)
    .collect(c => c.d)
    .getOrElse(false);
```

For enhanced exceptions, try:
```
type A = {
    b: boolean;
};

const supplier = () => {
    throw new Error('test error');
};

const t = TryFactory.of<A>(supplier);

const result = t.getOrElse({ b: false }); // holds false
```