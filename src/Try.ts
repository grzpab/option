import { Supplier } from './Supplier';

export type TryMapper<T extends Object, U extends Object> = (t: T) => Try<U>;

export interface Try<T extends Object> {
    getOrElse(t: T): T;
    get(): T;
    map<U extends Object>(mapper: TryMapper<T, U> ): Try<U>;
}

class TrySuccess<T extends Object> implements Try<T> {
    private _t: T;

    constructor(t: T) {
        this._t = t;
    }

    public getOrElse(t: T): T {
        return this._t;
    }

    public get(): T {
        return this._t;
    }

    public map<U extends Object>(mapper: TryMapper<T, U> ): Try<U> {
        return mapper(this._t);
    }
}

class TryFailure<T extends Object> implements Try<T> {
    private _error: Error;

    constructor(error: Error) {
        this._error = error;
    }

    getOrElse(t: T): T {
        return t;
    }

    public get(): T {
        throw this._error;
    }

    public map<U extends Object>(mapper: TryMapper<T, U> ): Try<U> {
        return new TryFailure<U>(this._error);
    }
}

export namespace TryFactory {
    export function of<T extends Object> (supplier: Supplier<T>): Try<T> {
        try {
            const t: T = supplier();

            return new TrySuccess<T>(t);
        } catch (_error ) {
            return new TryFailure<T>(_error);
        }
    }

    export function reduce<T extends Object>(t: Try<Try<T>>): Try<T> {
        return t.map(a => a);
    }
}