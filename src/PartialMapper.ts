import { Optional } from './Optional';

// a.k.a. PartialFunction (especially in mathematics)
export type PartialMapper<T, U> = (t: T) => Optional<U>;