import { Optional } from './Optional';
import { PartialMapper } from './PartialMapper';
import { Mapper } from './Mapper';
import { Predicate } from './Predicate';
import { Consumer } from './Consumer';
import { Supplier } from './Supplier';

export interface Option<T extends Object> {
    collect<U>(partialMapper: PartialMapper<T, U>): Option<U>;
    filter(predicate: Predicate<T>): Option<T>;
    map<U extends Object>(mapper: Mapper<T, U> ): Option<U>;
    flatMap<U extends Object>(mapper: Mapper<T, Option<U>>): Option<U>;
    getOrElse(t: T): T;
    getOrElseSupply(supplier: Supplier<T>): T;
    getOrElseThrow<E extends Error>(supplier: Supplier<E>): T;
    orElse(other: Option<T>): Option<T>;
    peek(consumer: Consumer<T>): Option<T>;
    transform<U>(mapper: Mapper<T, U>): U;
}

class OptionNone<T extends Object> implements Option<T> {
    public collect<U>(partialMapper: PartialMapper<T, U>): Option<U> {
        return new OptionNone<U>();
    }

    public filter(predicate: Predicate<T>): Option<T> {
        return this;
    }

    public map<U extends Object>(mapper: Mapper<T, U> ): Option<U> {
        return new OptionNone<U>();
    }
    
    public flatMap<U extends Object>(mapper: Mapper<T, Option<U>>): Option<U> {
        return new OptionNone<U>();
    }

    public getOrElse(t: T): T {
        return t;
    }

    public getOrElseSupply(supplier: Supplier<T>): T {
        return supplier();
    }

    public getOrElseThrow<E extends Error>(supplier: Supplier<E>): T {
        throw supplier();
    }

    public orElse(other: Option<T>): Option<T> {
        return other;
    }

    public peek(consumer: Consumer<T>): Option<T> {
        return this;
    }

    public transform<U>(mapper: Mapper<T, U>): U {
        throw new Error('cannot transform an OptionNone');
    }
}

class OptionSome<T extends Object> implements Option<T> {
    private _t: T;

    public constructor(t: T) {
        this._t = t;
    }

    public collect<U>(partialMapper: PartialMapper<T, U>): Option<U> {
        const u: Optional<U> = partialMapper(this._t);

        if (u === null || u === undefined) {
            return new OptionNone<U>();
        }
        
        return new OptionSome<U>(u);
    }

    public filter(predicate: Predicate<T>): Option<T> {
        return predicate(this._t) ? this : new OptionNone<T>();
    }

    public map<U extends Object>(mapper: Mapper<T, U> ): Option<U> {
        const u: U = mapper(this._t);

        return new OptionSome<U>(u);
    }

    public flatMap<U extends Object>(mapper: Mapper<T, Option<U>>): Option<U> {
        return mapper(this._t);
    }

    public getOrElse(t: T): T {
        return this._t;
    }

    public getOrElseSupply(supplier: Supplier<T>): T {
        return this._t;
    }

    public getOrElseThrow<E extends Error>(supplier: Supplier<E>): T {
        return this._t;
    }

    public orElse(other: Option<T>): Option<T> {
        return this;
    }

    public peek(consumer: Consumer<T>): Option<T> {
        consumer(this._t);

        return this;

    }

    public transform<U>(mapper: Mapper<T, U>): U {
        return mapper(this._t);
    }
}

export namespace OptionFactory {
    export function of<T extends Object>(t: T | null | undefined): Option<T> {
        if (t === null || t === undefined) {
            return new OptionNone<T>();
        }
        return new OptionSome<T>(t);
    }

    export function narrow<T extends Object, U extends T>(option: Option<U>): Option<T> {
        return option;
    }

    export function some<T extends Object>(t: T): Option<T> {
        return new OptionSome<T>(t);
    }

    export function none<T extends Object>(): Option<T> {
        return new OptionNone<T>();
    }

    export function whenSupplier<T extends Object>(condition: boolean, supplier: Supplier<T>): Option<T> {
        if (condition) {
            return new OptionSome<T>(supplier());
        }

        return new OptionNone<T>();
    }

    export function whenValue<T extends Object>(condition: boolean, value: T): Option<T> {
        if (condition) {
            return new OptionSome<T>(value);
        }

        return new OptionNone<T>();
    }
}